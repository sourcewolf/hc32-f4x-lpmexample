#include "Main_include.h"
#include "bsp_rmu.h"
uint32_t flag0,flag1;
uint16_t Wakeup_Source;
uint16_t ResetFlag;
uint8_t txdata[10]={1,2,3,4,5,6,7,8,9,0};
#define Ret_RAM_ADDR 0x200F0000
uint32_t Readback[10];
stc_clk_freq_t Clkdata;
void SystemEnterLowSpeedMode(void)
{
    CLK_SetSysClkSource(ClkSysSrcMRC);
    PWC_HP2LS();
    CLK_MpllCmd(Disable);
}
void SystemBackToHighSpeedMode(void)
{
    PWC_LS2HS();
    CLK_MpllCmd(Enable);
    while(Set != CLK_GetFlagStatus(ClkFlagMPLLRdy));
    CLK_SetSysClkSource(CLKSysSrcMPLL);
}
int main(void)
{
    uint32_t *pdata = (uint32_t *)Ret_RAM_ADDR;
    system_clk_init();
    CLK_GetClockFreq(&Clkdata);
    Ddl_UartInit();
    PORT_DebugPortSetting(0x1C,Disable);
    printf("read back: \r\n");
    for(int i= 0;i<10;i++)
    {
        printf("%x",pdata[i]);
    }
    printf("\r\n");
    for(int i= 0;i<10;i++)
    {
        pdata[i] = i;
    }
    bsp_rtc_init();
    /*获取复位源*/
    bsp_show_resetcause(bsp_get_resetcause());
    /*获取PowerDown唤醒源*/
    Wakeup_Source = GetWakeupFlag();
    printf("Wakeup source Flag WKF1,WKF0= %x\r\n",Wakeup_Source);
    printf("system init\r\n");
//    bsp_pvd1_config();//PVD1 configurate
    SystemEnterLowSpeedMode();
    Lowspeed_div();
    CLK_GetClockFreq(&Clkdata);
    Ddl_Delay1ms(5000);
    SystemBackToHighSpeedMode();
    hightspeed_div();
    CLK_GetClockFreq(&Clkdata);
    LPM_TEST();
    while(1)
    {
        printf("main loop\r\n");
        Ddl_Delay1ms(100);
    }
}
