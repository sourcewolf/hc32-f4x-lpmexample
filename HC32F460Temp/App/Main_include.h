#ifndef MAIN_INCLUDE_H
#define MAIN_INCLUDE_H
#include "hc32_ddl.h"
#include "system_Clk.h"
#include "System_PowerDown.h"
#include "bsp_rtc.h"
#include "bsp_wtm.h"
#include "bsp_eventport.h"
#include "bsp_pvd.h"
#include "bsp_i2c_slave_dma.h"
#endif
