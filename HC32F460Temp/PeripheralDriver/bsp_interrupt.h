#ifndef BSP_INTERRUPT_H
#define BSP_INTERRUPT_H
#include "hc32_ddl.h"
#define CAN_INT_IRQn            Int000_IRQn
#define XTAL_FAULT_IRQn         Int002_IRQn
#define DMA2_CH0_IRQn           Int003_IRQn
#define SPI3_TX_IRQn            Int004_IRQn
#define SPI3_RX_IRQn            Int005_IRQn
#define SPI3_ERR_IRQn           Int006_IRQn
#define SPI3_IDEL_IRQn          Int007_IRQn
#define TMRA1_OVF_IRQn          Int008_IRQn
#define I2S3_EER_IRQn           Int009_IRQn
#define I2C1_EEI_IRQn           Int010_IRQn
#define I2C1_TEI_IRQn           Int011_IRQn
#define I2C1_TXI_IRQn           Int012_IRQn
#define I2C1_RXI_IRQn           Int013_IRQn
#define Key0_IRQn               Int014_IRQn
#define Key1_IRQn               Int015_IRQn
#define ADC1_EOCA_IRQn          Int016_IRQn
#define ADC2_EOCA_IRQn          Int017_IRQn
#define ADC1_EOCB_IRQn          Int018_IRQn
#define ADC2_EOCB_IRQn          Int019_IRQn
#define TIMERA1_IRQn            Int020_IRQn
#define EVENTPORT1_IRQn         Int021_IRQn
#define BSP_PVD1_IRQn           Int022_IRQn
#define WTM_IRQn                Int023_IRQn
#define RTC_PRD_IRQn            Int024_IRQn
#define RTC_ALM_IRQn            Int029_IRQn

#define KEY_WK0_IRQn            Int025_IRQn
#define KEY_WK1_IRQn            Int026_IRQn
#define KEY_WK2_IRQn            Int027_IRQn
#define KEY_WK3_IRQn            Int028_IRQn
#ifdef __cplusplus
extern "C" {
#endif
void bsp_interrupt_callback_regist(en_int_src_t enIntSrc, IRQn_Type enIRQn, void *callback);
void bsp_interrupt_enable(IRQn_Type enIRQn, bool value);
void bsp_set_interrupt_priority(IRQn_Type enIRQn, uint32_t priority);
#ifdef __cplusplus
};
#endif    
    
#endif


