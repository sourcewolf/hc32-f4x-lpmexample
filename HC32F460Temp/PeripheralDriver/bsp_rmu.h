#ifndef BSP_RMU_H
#define BSP_RMU_H
#include "hc32_ddl.h"

#define RMU_RST_POWER_ON        0x00000001
#define RMU_RST_RESET_PIN       0x00000002
#define RMU_RST_BROWN_OUT       0x00000004
#define RMU_RST_PVD1            0x00000008
#define RMU_RST_PVD2            0x00000010
#define RMU_RST_WDT             0x00000020
#define RMU_RST_SWDT            0x00000040
#define RMU_RST_POWER_DOWN      0x00000080
#define RMU_RST_SOFTWARE        0x00000100
#define RMU_RST_MPU_ERR         0x00000200
#define RMU_RST_RAM_PARITY_ERR  0x00000400
#define RMU_RST_RAM_ECC         0x00000800
#define RMU_RST_CLK_ERR         0x00001000
#define RMU_RST_XTAL_ERR        0x00002000
#define RMU_RST_MULTI           0x00004000








#ifdef __cplusplus
extern "C" {
#endif


uint32_t bsp_get_resetcause(void);
void bsp_show_resetcause(uint32_t resetflag);


#ifdef __cplusplus
};
#endif


#endif
