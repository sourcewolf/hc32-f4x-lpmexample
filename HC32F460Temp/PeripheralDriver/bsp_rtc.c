#include "bsp_rtc.h"
#include "bsp_interrupt.h"
void bsp_rtc_prd_callback(void)
{
    PWC_IrqClkRecover();
    
    printf("RTC prd callback \r\n");
}
void bsp_rtc_alm_callback(void)
{
    printf("bsp_rtc_alm_callback \r\n");
}

uint8_t bsp_rtc_init(void)
{
    stc_rtc_init_t rtc_cfg;
    stc_rtc_date_time_t rtc_time;
    MEM_ZERO_STRUCT(rtc_time);
    MEM_ZERO_STRUCT(rtc_cfg);
    rtc_cfg.enClkSource = RtcClkXtal32;
    rtc_cfg.enCompenEn = Disable;
    rtc_cfg.enPeriodInt = RtcPeriodIntOneSec;
    rtc_cfg.enTimeFormat = RtcTimeFormat24Hour;
    RTC_Init(&rtc_cfg);
    rtc_time.enAmPm = RtcHour12Am;
    rtc_time.u8Year = 21;
    rtc_time.u8Month = 1;
    rtc_time.u8Day = 18;
    rtc_time.u8Hour = 10;
    rtc_time.u8Minute = 25;
    rtc_time.u8Second = 0;
    rtc_time.u8Weekday = 1;
    RTC_SetDateTime(RtcDataFormatDec,&rtc_time,Enable, Enable);
    RTC_IrqCmd(RtcIrqPeriod,Enable);
    RTC_IrqCmd(RtcIrqAlarm,Enable);
    RTC_Cmd(Enable);
    bsp_interrupt_callback_regist(INT_RTC_ALM,RTC_ALM_IRQn,(void *)bsp_rtc_alm_callback);
    bsp_interrupt_callback_regist(INT_RTC_PRD,RTC_PRD_IRQn,(void *)bsp_rtc_prd_callback);
    return 0;
}
uint8_t bsp_rtc_set_alm(void)
{
    stc_rtc_date_time_t curtime;
    stc_rtc_alarm_time_t almtime;
    RTC_GetDateTime(RtcDataFormatDec,&curtime);
    almtime.u8Weekday = 0x7F;
    almtime.u8Hour = curtime.u8Hour;
    if(curtime.u8Minute != 59)
    {
        almtime.u8Minute = curtime.u8Minute+1;
    }
    else
    {
        almtime.u8Hour += 1;
        almtime.u8Minute = 0;
    }
    RTC_SetAlarmTime(RtcDataFormatDec,&almtime);
    return 0;
}