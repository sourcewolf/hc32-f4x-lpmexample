#include "bsp_wtm.h"
#include "bsp_interrupt.h"


uint8_t bsp_wtm_init(void)
{
    stc_pwc_wktm_ctl_t wtm_cfg;
    MEM_ZERO_STRUCT(wtm_cfg);
    wtm_cfg.enWkclk = Wk64hz;
    wtm_cfg.enWkOverFlag = Equal;
    wtm_cfg.u16WktmCmp = 0x100;
    wtm_cfg.enWktmEn = Enable;
    PWC_WktmControl(&wtm_cfg);
    
    return 0;
}
uint8_t bsp_wtm_stop(void)
{
    stc_pwc_wktm_ctl_t wtm_cfg;
    MEM_ZERO_STRUCT(wtm_cfg);
    wtm_cfg.enWkclk = Wk64hz;
    wtm_cfg.enWkOverFlag = Equal;
    wtm_cfg.u16WktmCmp = 0xFFF;
    wtm_cfg.enWktmEn = Disable;
    PWC_WktmControl(&wtm_cfg);    
    return 0;
}


