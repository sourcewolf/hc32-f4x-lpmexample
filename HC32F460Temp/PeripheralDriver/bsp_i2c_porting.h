#ifndef BSP_I2C_PORTING_H
#define BSP_I2C_PORTING_H

#define I2C1_UNIT M4_I2C1
#define I2C2_UNIT M4_I2C2
#define I2C3_UNIT M4_I2C3

#define I2C_SLAVE_CH I2C1_UNIT

#define I2C1_SCL_PORT   PortC//PortA//PortC
#define I2C1_SCL_Pin    Pin04//Pin01//Pin04
#define I2C1_SCL_FUN    Func_I2c1_Scl
#define I2C1_SDA_PORT   PortC//PortA//PortC
#define I2C1_SDA_Pin    Pin05//Pin00//Pin05
#define I2C1_SDA_FUN    Func_I2c1_Sda

#define I2C2_SCL_PORT   PortD
#define I2C2_SCL_Pin    Pin00
#define I2C2_SCL_FUN    Func_I2c2_Scl
#define I2C2_SDA_PORT   PortD
#define I2C2_SDA_Pin    Pin01
#define I2C2_SDA_FUN    Func_I2c2_Sda

#define I2C1_CLK			PWC_FCG1_PERIPH_I2C1
#define I2C2_CLK			PWC_FCG1_PERIPH_I2C2
#define I2C3_CLK			PWC_FCG1_PERIPH_I2C3

#define I2C_DMA_UNIT    M4_DMA1
#define DMA_RX_CH       (DmaCh0)
#define DMA_TX_CH       (DmaCh1)
#define DMA_RX_CH_TRIG  EVT_I2C1_RXI
#define DMA_TX_CH_TRIG  EVT_I2C1_TXI
#define DMA_TX_INT      INT_DMA1_TC1
#define DMA_RX_INT      INT_DMA1_TC0
#define DMA_Mode_RXsrc  AddressFix
#define DMA_Mode_RXdes  AddressIncrease
#define DMA_Mode_TXsrc  AddressIncrease
#define DMA_Mode_TXdes  AddressFix
#define DataWidth       Dma8Bit



#define DMA_CH0_IRQn       Int015_IRQn
#define DMA_CH1_IRQn       Int016_IRQn

#define I2C_BAUDRATE       (400000u)
#define TIMEOUT            ((uint32_t)0x2000)

#define I2C_RET_OK                      0
#define I2C_RET_ERROR                   1
#define I2C_BUSY						2
#define I2C_TIMEROUT					3
#define I2C_BADADDR						4
#define I2C_BADPARA                     5//��������

/* Define slave device address for example */
#define SLAVE_ADDRESS                   0x06u

#define SLAVE_I2C_DMA_UNIT    M4_DMA2
#define SLAVE_DMA_RX_CH       (DmaCh0)
#define SLAVE_DMA_TX_CH       (DmaCh1)
#define SLAVE_DMA_RX_CH_TRIG  EVT_I2C1_RXI
#define SLAVE_DMA_TX_CH_TRIG  EVT_I2C1_TXI
#define SLAVE_DMA_TX_INT      INT_DMA2_TC1
#define SLAVE_DMA_RX_INT      INT_DMA2_TC0
#define SLAVE_DMA_Mode_RXsrc  AddressFix
#define SLAVE_DMA_Mode_RXdes  AddressIncrease
#define SLAVE_DMA_Mode_TXsrc  AddressIncrease
#define SLAVE_DMA_Mode_TXdes  AddressFix
#define SLAVE_DataWidth       Dma8Bit

/* Define Write and read data length for the example */
#define SLAVE_DATA_LEN                   64u

#endif
