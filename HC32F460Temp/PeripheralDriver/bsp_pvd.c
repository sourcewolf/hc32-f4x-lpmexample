#include "bsp_pvd.h"
#include "bsp_interrupt.h"
static void bsp_pvd1_callback(void)
{
    PWC_IrqClkRecover();
    printf("pvd1 callback \r\n");
}
void bsp_pvd1_config(void)
{
    stc_pwc_pvd_cfg_t pvdcfg;
    MEM_ZERO_STRUCT(pvdcfg);
//    pvdcfg.enPvd1Filtclk = PvdLrc1;
    pvdcfg.enPvd1FilterEn = Disable;
    pvdcfg.enPvd1Int = MskInt;//�������ж�
    pvdcfg.enPvd1Level = Pvd1Level3;//2.5V
    pvdcfg.stcPvd1Ctl.enPvdMode = PvdInt;
    pvdcfg.stcPvd1Ctl.enPvdCmpOutEn = Enable;
    pvdcfg.stcPvd1Ctl.enPvdIREn = Enable;
    PWC_PvdCfg(&pvdcfg);
    PWC_Pvd1Cmd(Enable);
    bsp_interrupt_callback_regist(INT_PVD_PVD1,BSP_PVD1_IRQn,(void *)bsp_pvd1_callback);    
}




