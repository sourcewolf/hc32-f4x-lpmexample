#include "bsp_eventport.h"
#include "bsp_interrupt.h"
static void eventport_callback(void)
{
    printf("eventport_callback \r\n");
}
void eventport_init(void)
{
    stc_event_port_init_t eventport_cfg;
    MEM_ZERO_STRUCT(eventport_cfg);
    eventport_cfg.enDirection = EventPortIn;//输入
    eventport_cfg.enFallingDetect = Enable;//下降沿事件有效
    eventport_cfg.enRisingDetect = Disable;//上升沿事件无效
    eventport_cfg.enFilter = Enable;
    eventport_cfg.enFilterClk = Pclk1Div8;
    PWC_Fcg0PeriphClockCmd(PWC_FCG0_PERIPH_AOS,Enable);
    PORT_SetFunc(PortA,Pin10,Func_Evnpt,Disable);
    EVENTPORT_Init(EventPort1,EventPin10,&eventport_cfg);
    bsp_interrupt_callback_regist(INT_EVENT_PORT1,EVENTPORT1_IRQn,(void *)eventport_callback);
}


