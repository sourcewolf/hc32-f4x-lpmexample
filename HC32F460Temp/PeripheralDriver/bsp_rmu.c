#include "bsp_rmu.h"
//#include "bsp_log.h"
#define LOG printf
uint32_t bsp_get_resetcause(void)
{
    uint32_t resetcause = M4_SYSREG->RMU_RSTF0;
    RMU_ClrResetFlag();
    return resetcause;
}
void bsp_show_resetcause(uint32_t resetflag)
{
    if(resetflag & RMU_RST_POWER_ON)
    {
        LOG("POR reset\r\n");
    }
    if(resetflag & RMU_RST_RESET_PIN)
    {
        LOG("Extern Pin Reset\r\n");
    }
    if(resetflag & RMU_RST_BROWN_OUT)
    {
        LOG("BOD Reset\r\n");
    }
    if(resetflag & RMU_RST_PVD1)
    {
        LOG("PVD1 Reset\r\n");
    }
    if(resetflag & RMU_RST_PVD2)
    {
        LOG("PVD2 Reset\r\n");
    }
    if(resetflag & RMU_RST_WDT)
    {
        LOG("WDT Reset\r\n");
    }
    if(resetflag & RMU_RST_SWDT)
    {
        LOG("SWDT Reset\r\n");
    }
    if(resetflag & RMU_RST_POWER_DOWN)
    {
        LOG("PowerDown Reset\r\n");
    }
    if(resetflag & RMU_RST_SOFTWARE)
    {
        LOG("Software Reset\r\n");
    }
    if(resetflag & RMU_RST_MPU_ERR)
    {
        LOG("MPU Error Reset\r\n");
    }
    if(resetflag & RMU_RST_RAM_PARITY_ERR)
    {
        LOG("RAM PARITY Error Reset\r\n");
    }
    if(resetflag & RMU_RST_RAM_ECC)
    {
        LOG("RAM ECC Error Reset\r\n");
    }
    if(resetflag & RMU_RST_CLK_ERR)
    {
        LOG("CLK Error Reset\r\n");
    }
    if(resetflag & RMU_RST_XTAL_ERR)
    {
        LOG("XTAL Error Reset\r\n");
    }
    if(resetflag & RMU_RST_MULTI)
    {
        LOG("mulitple Reset\r\n");
    }
}