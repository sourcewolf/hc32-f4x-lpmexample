#include "System_PowerDown.h"
//#include "System_InterruptCFG_Def.h"
#include "bsp_interrupt.h"

/**
 *******************************************************************************
 ** \brief System_Enter_StopMode
 **
 ** \param  None
 **
 ** \retval None
 **
 ******************************************************************************/
void System_Enter_StopMode(void)
{
    stc_pwc_stop_mode_cfg_t stcPwcStopCfg;

    MEM_ZERO_STRUCT(stcPwcStopCfg);

    /* Config stop mode. */
    stcPwcStopCfg.enStpDrvAbi = StopUlowspeed;//StopUlowspeed;
    stcPwcStopCfg.enStopClk = ClkFix;
    stcPwcStopCfg.enStopFlash = Wait;
    stcPwcStopCfg.enPll = Enable;
    PWC_StopModeCfg(&stcPwcStopCfg);
    PWC_EnterStopMd();
}
/**
 *******************************************************************************
 ** \brief GetWakeupFlag
 **
 ** \param  None
 **
 ** \retval None
 **
 ******************************************************************************/
uint16_t GetWakeupFlag(void)
{
    uint16_t temp;
    temp = M4_SYSREG->PWR_PDWKF0;
    temp += M4_SYSREG->PWR_PDWKF1<<8;
    PWC_ClearWakeup1Flag(0xFF);
    PWC_ClearWakeup0Flag(0xFF);
    return temp;
}
/**
 *******************************************************************************
 ** \brief WakeupSourceEnable
 **
 ** \param  None
 **
 ** \retval None
 **
 ******************************************************************************/
static void WakeupSourceEnable(void)
{
    PWC_PdWakeup0Cmd(KEY_WK1_EN,Enable);
    PWC_PdWakeup2Cmd(PWC_PDWKEN2_RTCPRD, Enable);
}
/**
 *******************************************************************************
 ** \brief System_Enter_PowerDown
 **
 ** \param  None
 **
 ** \retval None
 **
 ******************************************************************************/
void System_Enter_PowerDown(void)
{
    stc_pwc_pwr_mode_cfg_t  stcPwcPwrMdCfg;
    /* Config power down mode. */
    stcPwcPwrMdCfg.enPwrDownMd = PowerDownMd2;//PD2模式
    stcPwcPwrMdCfg.enRLdo = Enable;//LDO使能
    stcPwcPwrMdCfg.enIoRetain = IoPwrDownRetain;//掉电模式IO保持
    stcPwcPwrMdCfg.enRetSram = Enable;//RetSRAM保持
    stcPwcPwrMdCfg.enVHrc = Enable;//HRC不关闭
    stcPwcPwrMdCfg.enVPll = Enable;//PLL不关闭
    stcPwcPwrMdCfg.enRunDrvs =  RunHighspeed;//高速驱动能力
    stcPwcPwrMdCfg.enDrvAbility = HighSpeed;//高速驱动电压
    stcPwcPwrMdCfg.enPwrDWkupTm = Vcap0047;//VCAP电容0.047uF
    PWC_PowerModeCfg(&stcPwcPwrMdCfg);
    PWC_EnterPowerDownMd();//进入Powerdown模式
}
/**
 *******************************************************************************
 ** \brief KeyWakeup0_Callback
 **
 ** \param  None
 **
 ** \retval None
 **
 ******************************************************************************/
static void KeyWakeup0_Callback(void)
{
	PWC_IrqClkRecover();
    printf("wakeup key0\r\n");
    if (Set == EXINT_IrqFlgGet(KEY_WK0_EICH))
    {
        /* clear int request flag */
        EXINT_IrqFlgClr(KEY_WK0_EICH);
    }
}
#if 0
/**
 *******************************************************************************
 ** \brief KeyWakeup1_Callback
 **
 ** \param  None
 **
 ** \retval None
 **
 ******************************************************************************/
static void KeyWakeup1_Callback(void)
{
	PWC_IrqClkRecover();
    if (Set == EXINT_IrqFlgGet(KEY_WK1_EICH))
    {
        /* clear int request flag */
        EXINT_IrqFlgClr(KEY_WK1_EICH);
    }
}
/**
 *******************************************************************************
 ** \brief KeyWakeup2_Callback
 **
 ** \param  None
 **
 ** \retval None
 **
 ******************************************************************************/
#if 1
static void KeyWakeup2_Callback(void)
{
	PWC_IrqClkRecover();
    if (Set == EXINT_IrqFlgGet(KEY_WK2_EICH))
    {
        /* clear int request flag */
        EXINT_IrqFlgClr(KEY_WK2_EICH);
    }
}
/**
 *******************************************************************************
 ** \brief KeyWakeup3_Callback
 **
 ** \param  None
 **
 ** \retval None
 **
 ******************************************************************************/
static void KeyWakeup3_Callback(void)
{
	PWC_IrqClkRecover();
    if (Set == EXINT_IrqFlgGet(KEY_WK3_EICH))
    {
        /* clear int request flag */
        EXINT_IrqFlgClr(KEY_WK3_EICH);
    }
}
#endif
#endif
/**
 *******************************************************************************
 ** \brief Key_Wakeup_Inits
 **
 ** \param  None
 **
 ** \retval None
 **
 ******************************************************************************/
void Key_Wakeup_Init(void)
{
    stc_exint_config_t stcExtiConfig;
    stc_irq_regi_conf_t stcIrqRegiConf;
    stc_port_init_t stcPortInit;
    
    /* configuration structure initialization */
    MEM_ZERO_STRUCT(stcExtiConfig);
    MEM_ZERO_STRUCT(stcIrqRegiConf);
    MEM_ZERO_STRUCT(stcPortInit);
    
    /**************************************************************************/
    /* External Int                                           */
    /**************************************************************************/
    
    /* Filter setting */
    stcExtiConfig.enFilterEn = Enable;
    stcExtiConfig.enFltClk = Pclk3Div64;
    /* falling edge for keyscan function */
    stcExtiConfig.enExtiLvl = ExIntRisingEdge;
    stcExtiConfig.enExitCh = KEY_WK0_EICH;
    EXINT_Init(&stcExtiConfig);
    /* Set PD12 as External Int Ch.12 input */
    MEM_ZERO_STRUCT(stcPortInit);
    stcPortInit.enExInt = Enable;//使能中断
    stcPortInit.enPullUp = Disable;//使能内部上拉
    /*配置WKUP 端口属性*/
    PORT_Init(KEY_WK0_PORT, KEY_WK0_PIN, &stcPortInit);

    /* Select External Int Ch.01 */
    bsp_interrupt_callback_regist(KEY_WK0_INT,KEY_WK0_IRQn,(void *)KeyWakeup0_Callback);   
}
void System_Clk_8MHZ(void)
{
    stc_pwc_pwr_mode_cfg_t PWR_mode;
    MEM_ZERO_STRUCT(PWR_mode);
    CLK_MrcCmd(Enable);
    CLK_SetSysClkSource(ClkSysSrcMRC);
    PWR_mode.enDrvAbility = Ulowspeed;
    //PWR_mode.enDynVol = Voltage09;
    PWR_mode.enRunDrvs = RunUlowspeed;
    PWC_PowerModeCfg(&PWR_mode);
}
void  PowertoHighspeed(void)
{
    stc_pwc_pwr_mode_cfg_t PWR_mode;
    MEM_ZERO_STRUCT(PWR_mode);
    PWR_mode.enDrvAbility = HighSpeed;
    PWR_mode.enRunDrvs = RunHighspeed;
    PWC_PowerModeCfg(&PWR_mode);
}
void LPM_TEST(void)
{
    Ddl_Delay1ms(2000);
/*------step 1------------config interrupts------------------------*/    
    Key_Wakeup_Init();
    printf("System Enter Stop!\r\n");
    
/*------step 2------------Enable Stop mode wakup source------------------------*/        
    enIntWakeupEnable(Extint11WU);
    enIntWakeupEnable(Vdu1WU);
    enIntWakeupEnable(RtcPeriodWU);
/*-----step 3------------Config and Enter stop mode------------------------*/     
    System_Enter_StopMode(); 
    printf("System_Wakeup from stop mode\r\n");
    if(PWC_GetPvdFlag(PvdU1) == Set)
    {
        printf("wakeup by pvd1\r\n");
        PWC_ClearPvdFlag(PvdU1);
    }
    
    /*step 4-7 is configurate powerdown mode*/    
/*-----step 4------------Config WAKUP PIN------------------------*/         
    PWC_PdWakeupEvtEdgeCfg(PWC_PDWKUP_EDGE_WKP1,EdgeFalling);
/*-----step 5------------Enable Powerdown mode wakup source-----------------------*/     
    WakeupSourceEnable();
/*-----step 6------------Clear all the wakup source-----------------------*/     
    PWC_ClearWakeup1Flag(0xFF);
    PWC_ClearWakeup0Flag(0xFF);
    CLK_LrcCmd(Disable);//不使用内部LRC时，把LRC关闭，降低功耗
    SWDT_RefreshCounter();
    printf("System Enter Powerdown!\r\n");
/*-----step 7------------Config and Enter powerdwon mode------------------------*/         
    System_Enter_PowerDown(); //进入Powerdown模式
}

