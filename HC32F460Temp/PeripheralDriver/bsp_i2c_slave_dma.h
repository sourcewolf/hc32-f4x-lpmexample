#ifndef BSP_I2C_SLAVE_DMA_H
#define BSP_I2C_SLAVE_DMA_H
#include "hc32_ddl.h"
#include "bsp_dma.h"
#include "bsp_i2c_porting.h"
/*******************************************************************************
 * Local pre-processor symbols/macros ('#define')
 ******************************************************************************/

#define TIMERA_UNIT_CLOCK(x)            PWC_FCG2_PERIPH_TIMA##x
#define TIMERA_UNIT(x)                  M4_TMRA##x

uint8_t bsp_i2c_slave_init(M4_I2C_TypeDef* I2Cx);
void slave_prepareTXdata(uint8_t addr, uint8_t *data ,uint8_t len);
void I2C_slave_init(void);
void TimerA1_Init(void);
void TimerA2_Init(void);
void slave_tx(uint8_t *data, uint16_t len);
void Update_TxBuffer(uint8_t *data);

#endif
